package org.coderfun.fieldmeta.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.coderfun.common.BaseEntity_;

@Generated(value="Dali", date="2018-05-24T13:50:31.707+0800")
@StaticMetamodel(TypeMapping.class)
public class TypeMapping_ extends BaseEntity_ {
	public static volatile SingularAttribute<TypeMapping, String> sqlDialectCode;
	public static volatile SingularAttribute<TypeMapping, String> fullJavaType;
	public static volatile SingularAttribute<TypeMapping, String> javaType;
	public static volatile SingularAttribute<TypeMapping, String> sqlType;
	public static volatile SingularAttribute<TypeMapping, String> needJoinColumn;
}
