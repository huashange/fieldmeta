package org.coderfun.fieldmeta.service;

import klg.j2ee.common.dataaccess.BaseService;
import org.coderfun.fieldmeta.entity.TypeMapping;

public interface TypeMappingService extends BaseService<TypeMapping, Long>{

}
