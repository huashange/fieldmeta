package org.coderfun.fieldmeta.dao;

import klg.j2ee.common.dataaccess.BaseRepository;
import org.coderfun.fieldmeta.entity.PageField;

public interface PageFieldDAO extends BaseRepository<PageField, Long> {

}
